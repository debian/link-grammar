link-grammar (5.12.5~dfsg-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * declare compliance with Debian Policy 4.7.0

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 10 Jun 2024 08:46:46 +0200

link-grammar (5.12.4~dfsg-2) unstable; urgency=high

  * Team upload
  * Source-only upload

 -- Jeremy Bícha <jbicha@ubuntu.com>  Sun, 05 May 2024 13:29:51 -0400

link-grammar (5.12.4~dfsg-1) unstable; urgency=medium

  [ upstream ]
  * new release(s)

  [ Jonas Smedegaard ]
  * update copyright info: update coverage
  * generally update java-related architecture constraints,
    semi-auto-resolved using new source helper script pkgarchs.sh;
    thanks to Samuel Thibault for nudging
  * update copyright info: update coverage
  * stop autopkgtest-depend on python3-distutils;
    closes: bug#1065897, thanks to Graham Inggs
  * rename libraries for 64-bit time_t transition;
    tighten build-dependency on d-shlibs to support option --t64;
    closes: bug#1062748, thanks to Steve Langasek
  * tighten cleanup after build;
    closes: bug#1047439, thanks to Lucas Nussbaum
  * build-depend on pkgconf (not pkg-config)

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 02 Apr 2024 14:28:09 +0200

link-grammar (5.12.3~dfsg-1) unstable; urgency=medium

  [ upstream ]
  * new release(s)

  [ Jonas Smedegaard ]
  * drop patch cherry-picked upstream now applied
  * update copyright info: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 27 Jun 2023 18:28:28 +0200

link-grammar (5.12.0~dfsg-2) unstable; urgency=medium

  * fix source helper tool copyright-check
    to avoid insecure shell expansion
  * simplify build rules: testsuite is now more verbose;
    thanks to Matthias Klose (see bug#1029233)
  * add patch cherry-picked upstream
    to allocate a per-thread copy of pcre2_match_data

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 24 Jan 2023 06:40:52 +0100

link-grammar (5.12.0~dfsg-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * fix source helper tool copyright-check
    to work with Path::Tiny 0.144
  * declare compliance with Debian Policy 4.6.2

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 07 Jan 2023 11:16:04 +0100

link-grammar (5.11.0~dfsg-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * drop all patch, now applied
  * update copyright info: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 04 Oct 2022 17:23:51 +0200

link-grammar (5.10.5~dfsg-2) unstable; urgency=medium

  * add patch cherry-picked upstream
    to find zlib using pkg-config
  * add patches cherry-picked upstream
    to bypass a version check bug in AX_PYTHON_DEVEL,
    superseding patch 2001;
    closes: bug#1019432, #1020176,
    thanks to Graham Inggs and Lucas Nussbaum
  * add debian/patches/README
    documenting patch versioning micro-policy
  * drop ancient breaks/replaces
  * update source helper script copyright-check
  * override more license-related lintian warnings

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 18 Sep 2022 12:20:09 +0200

link-grammar (5.10.5~dfsg-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * update copyright info:
    + fix typo
    + update coverage
  * declare compliance with Debian Policy 4.6.1
  * unfuzz patch

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 22 Jun 2022 23:47:26 +0200

link-grammar (5.10.4~dfsg-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * update copyright info: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 08 Mar 2022 03:03:08 +0100

link-grammar (5.10.3~dfsg-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * tighten lintian overrides
  * update copyright info: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 21 Feb 2022 22:18:01 +0100

link-grammar (5.10.2~dfsg-3) unstable; urgency=medium

  * add patch 2001
    to work around broken version check in ax_python_devel.m4;
    closes: bug#1004416, thanks to Graham Inggs

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 28 Jan 2022 10:53:23 +0100

link-grammar (5.10.2~dfsg-2) unstable; urgency=medium

  * revert to disable sh4 as java-supported architecture
  * build-depend on python3-dev (not python3-all-dev);
    closes: bug#998442, thanks to Graham Inggs
  * update copyright info: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 18 Nov 2021 23:32:19 +0100

link-grammar (5.10.2~dfsg-1) unstable; urgency=medium

  [ upstream ]
  * new release
    + fix size in brand-new `link-generator` (hits 32-bit & ARM);
      closes: bug#993847, thanks to Adrian Bunk

  [ Jonas Smedegaard ]
  * drop patches cherry-picked upstream now applied
  * drop obsolete unapplied patches
  * enable sh4 as java-supported architecture

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 16 Sep 2021 20:31:12 +0200

link-grammar (5.10.1~dfsg-3) unstable; urgency=medium

  * drop non-working workaround for bug#993847
  * add patches cherry-picked upstream
    to fix testsuite failing on 32bit archs;
    closes: bug#993847, thanks to Adrian Bunk

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 15 Sep 2021 14:54:25 +0200

link-grammar (5.10.1~dfsg-2) unstable; urgency=medium

  * conditionally avoid tests
    confirmed upstream to be broken on 32bit archs;
    closes: bug#993847, thanks to Adrian Bunk

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 15 Sep 2021 00:32:29 +0200

link-grammar (5.10.1~dfsg-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * stop build-depend on minisat, unused since 5.9.1
  * update copyright info:
    + add alternate GitHub source URI
    + use License-Grant and Reference;
      add lintian overrides about License-Reference field
    + consistently list License sections last, and sort alphabetically
    + normalize listing of copyright holders, and sort alphabetically
    + normalize listing of files
    + fix list file licensed ISC and LGPL-2.1
    + update coverage
  * add source script copyright-check
  * update copyright hints

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 14 Sep 2021 12:19:59 +0200

link-grammar (5.10.0~dfsg-1) unstable; urgency=medium

  [ upstream ]
  * new release

  * update copyright info:
    + repackage source, omitting source-less PDF file
  * update watch file: use repack suffix ~dfsg
  * drop obsolete python2 install hint file
  * declare compliance with Debian Policy 4.6.0

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 06 Sep 2021 19:46:42 +0200

link-grammar (5.9.1-1) experimental; urgency=medium

  [ upstream ]
  * new release(s)

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 12 Jun 2021 11:26:44 +0200

link-grammar (5.8.1-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * adopt package
  * update watch file:
    + simplify filenamemangle regex
    + use substitution strings
    + track tags (not releases)
    + add usage comment
  * update git-buildpackage config:
    + use DEP-14 branch names */latest
    + sign tags
    + filter out all .git* files
    + stop disable patch numbering
    + add usage comment
  * set Rules-Requires-Root: no
  * drop patches cherry-picked upstream now applied

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 25 Jan 2021 03:14:27 +0100

link-grammar (5.8.0-7) unstable; urgency=medium

  * QA upload.

  * add patches cherry-picked upstream
    to fix multi-dict test failure
    (see bug#975696)

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 03 Jan 2021 01:29:28 +0100

link-grammar (5.8.0-6) unstable; urgency=medium

  * QA upload.

  * drop unsuccessful patch 1001
  * add patches cherry-picked upstream
    to fix multi-thread test failure;
    closes: bug#975696

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 02 Jan 2021 00:30:14 +0100

link-grammar (5.8.0-5) unstable; urgency=medium

  * QA upload.

  * add patch 1001 as a possible multithreading fix

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 30 Dec 2020 14:21:33 +0100

link-grammar (5.8.0-4) unstable; urgency=medium

  * QA upload.

  * inspect test-suite log in case of failure,
    unless DEB_BUILD_OPTIONS=terse

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 29 Dec 2020 21:47:56 +0100

link-grammar (5.8.0-3) unstable; urgency=medium

  * QA upload.

  * improve autopkgtest:
    + check core tests and python bindings unit tests separately
    + tighten python bindings unit tests
      to check data and all (not only most) code
      from installed packaged (not from unpacked source)
    + flag core test as flaky
      (mysteriously fails particularly on armhf,
      despite succeeding during build);
      closes: bug#975696, thanks to Graham Inggs

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 29 Dec 2020 18:46:03 +0100

link-grammar (5.8.0-2) unstable; urgency=medium

  * QA upload.

  * rename patches;
    add debian/patches/README micro-policy;
    consistently add DEP-3 patch headers
  * support DEB_BUILD_OPTIONS=noopt:
    + add patches cherry-picked upstream
      to allow overriding upstream compiler option -O3
    + re-enable -O3 by default in rules file
  * run tests verbosely in autopkgtest
  * stop include unit tests with package python-link-grammar-examples,
    and stop check those tests with autopkgtest:
    superfluous and requires data from source package
  * fix build-depend on locales-all,
    and stop disable tests during build (seemingly works now)
  * build-depend on hunspell libpcre2-dev
  * tighten build-dependency on d-shlibs, and drop local overrides
  * use debhelper compatibility level 13 (not 12)
  * build-depend on dh-sequence-python3 python3-all-dev
    (not dh-python python3-dev python3-distutils
  * declare compliance with Debian Policy 4.5.1

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 27 Dec 2020 16:46:08 +0100

link-grammar (5.8.0-1) unstable; urgency=medium

  * QA upload.
  * New upstream release - closes: #962453

 -- Masayuki Hatta <mhatta@debian.org>  Sun, 23 Aug 2020 09:32:43 +0900

link-grammar (5.7.0-3) unstable; urgency=medium

  * QA upload.
  * Fix ftbfs with swig 4.0.

 -- Matthias Klose <doko@debian.org>  Mon, 24 Feb 2020 11:46:39 +0100

link-grammar (5.7.0-2) unstable; urgency=medium

  * QA upload.

  [ Matthias Klose ]
  * Allow stderr in python-tests autopkg test.

  [ Gianfranco Costamagna ]
  * debian/patches/incompatible-cxx-warnings.patch: drop the Werror option
    implicit-function-declaration from those passed to C++ compilers; since it
    is only valid for C.
  * debian/patches/fix-spell-guessing.patch: Give ourselves a few more guesses
    to find the right spelling for words. This should help tests. Dictionaries
    grow.
  * don't fail on new python autopkgtest, and put back the old autopkgtest

 -- Matthias Klose <doko@debian.org>  Thu, 13 Feb 2020 08:58:32 +0100

link-grammar (5.7.0-1ubuntu1) focal; urgency=medium

  * Merge from Debian unstable. Remaining changes:
   - debian/patches/incompatible-cxx-warnings.patch: drop the Werror option
     implicit-function-declaration from those passed to C++ compilers; since it
     is only valid for C.
   - debian/patches/fix-spell-guessing.patch: Give ourselves a few more guesses
     to find the right spelling for words. This should help tests. Dictionaries
     grow.
   - don't fail on new python autopkgtest, and put back the old autopkgtest

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Fri, 07 Feb 2020 16:18:53 +0100

link-grammar (5.7.0-1) unstable; urgency=medium

  [ Masayuki Hatta ]
  * QA upload.
  * Update standards version to 4.5.0, no changes needed.
  * Add 01-disable-multi-dict-test.patch to disable the multi-dict test.
    I'm still not sure why it works outside then fails inside pbuilder.

  [ Fabian Wolff ]
  * New upstream release.
  * Update watch file to use GitHub.
  * Upgrade to debhelper compat level 12.
  * Drop python-link-grammar package (Closes: #936951).
  * Add flex to build dependencies.
  * "make check" should run during the build; this is not what
    autopkgtest is intended for.
  * Add 00-disable-python-test.patch to disable the Python tests during
    the build (see the header for justification), and add python-tests
    autopkgtest to run the Python tests on the installed package
    instead.
  * Remove debian/liblink-grammar5.symbols (Closes: #939454)
    (admittedly not the best solution probably, but whoever has a
    better idea is welcome to fix this in the future).

  [ Debian Janitor ]
  * Re-export upstream signing key without extra signatures.
  * Remove obsolete fields Name from debian/upstream/metadata.

 -- Masayuki Hatta <mhatta@debian.org>  Mon, 03 Feb 2020 23:51:14 +0900

link-grammar (5.7.0-0ubuntu1) focal; urgency=medium

  * New upstream version
  * debian/liblink-grammar5.symbols:
    - new version update

 -- Sebastien Bacher <seb128@ubuntu.com>  Wed, 04 Dec 2019 16:58:49 +0100

link-grammar (5.6.2-1ubuntu1) eoan; urgency=medium

  * debian/patches/incompatible-cxx-warnings.patch: drop the Werror option
    implicit-function-declaration from those passed to C++ compilers; since it
    is only valid for C.
  * debian/patches/fix-spell-guessing.patch: Give ourselves a few more guesses
    to find the right spelling for words. This should help tests. Dictionaries
    grow.
  * debian/rules: soften the dpkg-gensymbols options to -c1; such that we don't
    fail to build because of C++ symbols.
  * debian/liblink-grammar5.symbols: drop mangled C++ symbols which will differ
    per architecture, etc.

 -- Mathieu Trudel-Lapierre <cyphermox@ubuntu.com>  Thu, 22 Aug 2019 12:14:23 -0400

link-grammar (5.6.2-1) unstable; urgency=medium

  [ Masayuki Hatta ]
  * QA upload.
  * New upstream release.
  * Bumped Standards-Version to 4.4.0.

  [ Jonas Smedegaard ]
  * Fix Vcs-Git URL.

 -- Masayuki Hatta <mhatta@debian.org>  Tue, 20 Aug 2019 22:09:58 -0700

link-grammar (5.5.1-6) unstable; urgency=medium

  * QA upload.
  * Update git-buildpackage config: Use debian branch debian/master.
  * Fix declare liblink-grammar4-dev dependencies.
    Build-depend on d-shlibs.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 22 Feb 2019 22:27:48 +0100

link-grammar (5.5.1-5) unstable; urgency=medium

  * QA upload.
  * Really disable Java for architectures without it, passing
    --disable-java-bindings to configure (which defaults to enabling Java).
  * Disable Java on kfreebsd-amd64, and kfreebsd-i386, as it is not building
    anymore.
  * Remove trailing whitespaces in changelog.
  * Remove unused ${shlibs:Depends} substvar from liblink-grammar-dev.

 -- Pino Toscano <pino@debian.org>  Mon, 24 Dec 2018 10:49:28 +0100

link-grammar (5.5.1-4) unstable; urgency=medium

  * QA upload
  * Add libsqlite3 dependency, to fix testsuite
    - Drop previous patch, the test now is good, thanks upstream for the hint
  * Add builddeps in debian/tests/control, to finally have all the required
    dependencies to run correctly testsuite (Closes: #915060)
    - Thanks Rene Engelhard for the hint

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Fri, 30 Nov 2018 06:33:19 +0100

link-grammar (5.5.1-3) unstable; urgency=medium

  * QA upload

  * configure with --with-hunspell-dictdir=/usr/share/hunspell since
    that's where the dictionaries are since ages.

 -- Rene Engelhard <rene@debian.org>  Thu, 29 Nov 2018 22:19:14 +0100

link-grammar (5.5.1-2) unstable; urgency=medium

  * QA upload
  * Disable failing test that needs test stuff installed on the system

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Wed, 28 Nov 2018 11:51:08 +0100

link-grammar (5.5.1-1) unstable; urgency=medium

  * QA upload
  * New upstream release
  * debian/liblink-grammar5.symbols: Update

 -- Jeremy Bicha <jbicha@debian.org>  Sun, 25 Nov 2018 18:34:49 -0500

link-grammar (5.5.0-1) unstable; urgency=medium

  * QA upload.
  * New upstream release.
  * Update symbols file.
  * Remove trailing whitespace from debian/changelog in order to
    silence the file-contains-trailing-whitespace Lintian tag.
  * Upgrade to debhelper compat version 11.
  * Remove the empty file debian/patches/series.
  * Use HTTPS URI in debian/watch.
  * Upgrade to Standards-Version 4.1.4 (no changes).
  * Add debian/python3-link-grammar.lintian-overrides to override the
    python-package-depends-on-package-from-other-python-variant
    Lintian tag.
  * Update Vcs-Browser and Vcs-Git fields in debian/control.
  * Update Homepage field in debian/control and Source field in
    debian/copyright to use HTTPS.
  * Remove incorrect Multi-Arch fields in debian/control.
  * Update debian/copyright.
  * Update debian/link-grammar.install.

 -- Fabian Wolff <fabi.wolff@arcor.de>  Mon, 30 Apr 2018 20:53:21 +0200

link-grammar (5.3.16-3) unstable; urgency=medium

  * QA upload.
  * Build-depend on python3-distutils.

 -- Matthias Klose <doko@debian.org>  Sun, 29 Apr 2018 22:37:59 +0200

link-grammar (5.3.16-2) unstable; urgency=medium

  * QA upload.
  * Apply patch from Steve Langasek adding the missing test
    dependency on default-jdk. (Closes: #865902)

 -- Adrian Bunk <bunk@debian.org>  Mon, 26 Jun 2017 08:37:55 +0300

link-grammar (5.3.16-1) unstable; urgency=medium

  * New upstream release
  * debian/docs: README was renamed to README.md

 -- Jeremy Bicha <jbicha@ubuntu.com>  Mon, 19 Jun 2017 12:42:30 -0400

link-grammar (5.3.14-1) unstable; urgency=medium

  * QA upload
  * New upstream release
  * debian/liblink-grammar5.symbols: Add new symbols
  * debian/link-grammar-dictionaries-en.install:
    - Install new "ady" language

 -- Jeremy Bicha <jbicha@ubuntu.com>  Thu, 19 Jan 2017 21:07:37 -0500

link-grammar (5.3.13-1) unstable; urgency=medium

  * QA upload
  * New upstream release
  * Add python3-link-grammar package
  * Move python examples to new python-link-grammar-examples package
  * Drop debian/patches/git_link-with-libstdcc.patch, applied in
    new version

 -- Jeremy Bicha <jbicha@ubuntu.com>  Sun, 18 Dec 2016 23:16:47 -0500

link-grammar (5.3.11-2) unstable; urgency=medium

  * QA upload
  * debian/rules:
    - Try to make the 'delete *.la' rule smarter to not break autopkgtest
    - Fix build failures on architectures that don't build
      liblink-grammar-java
  * Add debian/patches/git_link-with-libstdcc.patch:
    - Cherry-pick commit to try to fix build log check warning
      about not linking against libstdc++

 -- Jeremy Bicha <jbicha@ubuntu.com>  Tue, 04 Oct 2016 11:30:43 -0400

link-grammar (5.3.11-1) unstable; urgency=medium

  * QA upload
  * New upstream release
  * debian/control:
    - (Build-)Depend on minisat instead of using bundled copy
    - Build-Depend on zlib1g-dev
  * debian/rules:
    - Use dh_install --fail-missing

 -- Jeremy Bicha <jbicha@ubuntu.com>  Wed, 28 Sep 2016 09:09:26 -0400

link-grammar (5.3.10-1) unstable; urgency=medium

  * QA upload.
  * New upstream release.

 -- Jeremy Bicha <jbicha@ubuntu.com>  Wed, 14 Sep 2016 20:28:10 -0400

link-grammar (5.3.9-1) unstable; urgency=medium

  * QA upload
  * New upstream release
  * Bump dh compat to 10

 -- Jeremy Bicha <jbicha@ubuntu.com>  Mon, 29 Aug 2016 10:14:31 -0400

link-grammar (5.3.8-2) unstable; urgency=medium

  * QA upload
  * debian/rules:
    - Fix override_dh_auto_clean typo, thanks lintian 2.5.46!
  * debian/tests/control:
    - Depend on build-essential for autopkgtest build test

 -- Jeremy Bicha <jbicha@ubuntu.com>  Wed, 17 Aug 2016 11:18:59 -0400

link-grammar (5.3.8-1) unstable; urgency=medium

  * QA upload
  * New upstream release
    - Includes "reproducible builds" patch from Chris Lamb (Closes: #829011)
  * Drop all patches since they've been applied in new release
  * debian/tests/control: Add Restrictions: build-needed
  * Have autopkgtest run upstream python tests too
  * Add python-link-grammar.examples
  * Have python-link-grammar recommend link-grammar-dictionaries-all
    since it's needed for the examples

 -- Jeremy Bicha <jbicha@ubuntu.com>  Tue, 16 Aug 2016 16:27:14 -0400

link-grammar (5.3.7-2) unstable; urgency=medium

  * Build link-grammar-java for alpha but not m68k or sh4 (Closes: #828809)

 -- Jeremy Bicha <jbicha@ubuntu.com>  Tue, 28 Jun 2016 00:05:38 -0400

link-grammar (5.3.7-1) unstable; urgency=medium

  [ Jeremy Bicha ]
  * QA upload
  * New upstream release (Closes: #774088) (LP: #1406051)
  * Update d/copyright. link-grammar is now LGPL-2.1.
  * Update packages from link-grammar4 to 5
  * Rename liblink-grammar4-dev to liblink-grammar-dev
    and liblink-grammar4-java to liblink-grammar-java
  * Modernize packaging
    - Switch from cdbs to dh7
    - Set debian/compat to 9
    - Set debian/source/format to quilt (3.0)
    - Bump Standards-Version to 3.9.8
    - Enable all hardening flags
    - Add debian/upstream/signing-key.asc
    - Add debian/upstream/metadata
    - Update debian/watch
  * Run dh-autoreconf.
    - Build-depend on dh-autoreconf, autoconf-archive & pkg-config
    - Add 0001-fix-configure.patch to facilitate this.
  * Add python-link-grammar package
  * Add link-grammar-dictionaries-all package and have it replace
    the Lithuanian dictionary package
  * Build for hunspell not aspell as recommended upstream
  * Use autopkgtest to run upstream tests

  [ Gianfranco Costamagna ]
  * update copyright for python-example bindings and some more
    missing licenses.
  * Add dh-autoreconf

 -- Jeremy Bicha <jbicha@ubuntu.com>  Sun, 26 Jun 2016 03:08:59 -0400

link-grammar (4.7.4-3) unstable; urgency=medium

  * Use autotools-dev to support newer architectures (Closes: 765232)
  * Orphan package to QA, at maintainer's request
  * Fix typo in package description (Closes: 698587)
  * Update architectures for java JNI build.
  * Remove obsolete Dm-Upload-Allowed field

 -- Wookey <wookey@debian.org>  Sun, 31 May 2015 14:22:39 +0000

link-grammar (4.7.4-2) unstable; urgency=low

  * Update homepage (Closes: #618610)
  * Remove debhelper boilerplate from debian/watch

 -- Ken Bloom <kbloom@gmail.com>  Fri, 29 Apr 2011 08:35:45 -0500

link-grammar (4.7.4-1) unstable; urgency=low

  * New upstream release
  * Make binary package link-grammar Multi-Arch: foreign

 -- Ken Bloom <kbloom@gmail.com>  Fri, 25 Feb 2011 13:08:22 -0600

link-grammar (4.7.0-1) unstable; urgency=low

  * New upstream release.
  * Update standards version to 3.9.1

 -- Ken Bloom <kbloom@gmail.com>  Tue, 26 Oct 2010 23:19:25 -0500

link-grammar (4.6.7-1) unstable; urgency=low

  * New upstream release. (Closes: 576201)
  * Don't install /usr/lib/*.la files (Closes: 570633)
    Various commenters on #570633 tell me this works when rebuilding other
    packages against link-grammar, so I'm going to trust them unless someone
    reports another bug.
  * Update standards version to 3.8.4
  * Add debian/source/format = 1.0

 -- Ken Bloom <kbloom@gmail.com>  Wed, 05 May 2010 10:27:15 -0500

link-grammar (4.6.5-1) unstable; urgency=low

  * New upstream release
  * Set JAVA_HOME correctly during build.
  * Link with -Wl,--as-needed so that CLI and JNI library don't have
    direct dependencies on the spell checker.
  * Versioned depends of liblink-grammar4-dev on liblink-grammar4 fixes
    lintian error.
  * Update standards-version to 3.8.3 fixes lintian warning.
  * Add symbols file for shlibs
  * Update short descriptions so they don't match for all packages

 -- Ken Bloom <kbloom@gmail.com>  Wed, 18 Nov 2009 09:30:03 -0600

link-grammar (4.5.8-1) unstable; urgency=low

  * New upstream release.

 -- Ken Bloom <kbloom@gmail.com>  Thu, 13 Aug 2009 20:52:05 -0500

link-grammar (4.5.7-2) unstable; urgency=low

  * Fix bashism in debian/rules (Closes: 535405)

 -- Ken Bloom <kbloom@gmail.com>  Wed, 01 Jul 2009 21:39:50 -0500

link-grammar (4.5.7-1) unstable; urgency=low

  * New upstream release.
  * Bump standards version to 3.8.2 (No changes needed).

 -- Ken Bloom <kbloom@gmail.com>  Mon, 22 Jun 2009 13:12:01 -0500

link-grammar (4.5.6-1) unstable; urgency=low

  * New upstream release.

 -- Ken Bloom <kbloom@gmail.com>  Tue, 26 May 2009 10:05:50 -0500

link-grammar (4.5.5-1) unstable; urgency=low

  * New upstream release.
  * Fix /usr/share/java/link-grammar.jar symlink to not include the build dir
  * Update minimum debhelper build-dep to >= 7.0.0
  * Fix spelling errors in changelog, copyright

 -- Ken Bloom <kbloom@gmail.com>  Tue, 19 May 2009 21:02:38 -0500

link-grammar (4.5.3-1) unstable; urgency=low

  * New upstream release.
  * debian/control: fix an arch: line to make the line continuation correct
    so that liblink-grammar4-java really builds on all architectures that
    support it

 -- Ken Bloom <kbloom@gmail.com>  Fri, 17 Apr 2009 15:44:55 -0500

link-grammar (4.3.9-2) unstable; urgency=low

  * Don't pass -z noexecstack to the linker because it's overridden
    anyway, and it breaks compilation on several architectures

 -- Ken Bloom <kbloom@gmail.com>  Fri, 28 Nov 2008 09:48:34 -0600

link-grammar (4.3.9-1) unstable; urgency=low

  * New upstream release
  * Use default-jdk to build JNI package (Closes: #506209)
  * Don't build liblink-grammar-java on architectures that don't
    (currently) have default-jdk (Closes: #506531)
  * Update man page to refer to the binary as link-parser
    (Closes: #504538)
  * Fix debian/rules clean
  * Update to standards version 3.8.0 (no changes needed)
  * Add link-grammar-dictionaries-lt for the included Lithuanian
    dictionaries
  * Pass -z noexecstack to the linker

 -- Ken Bloom <kbloom@gmail.com>  Sun, 23 Nov 2008 11:16:00 -0600

link-grammar (4.3.5-1) unstable; urgency=low

  * New upstream release.
  * Remove Linda overrides, because Linda is obsolete.

 -- Ken Bloom <kbloom@gmail.com>  Sun, 22 Jun 2008 23:59:53 +0300

link-grammar (4.3.2-1) unstable; urgency=low

  * New upstream release.

 -- Ken Bloom <kbloom@gmail.com>  Mon, 04 Feb 2008 16:12:33 -0600

link-grammar (4.3.1-1) unstable; urgency=low

  * New upstream release
    - Adds JNI interface
    - Expanded dictionaries
  * Add a watch file

 -- Ken Bloom <kbloom@gmail.com>  Fri, 01 Feb 2008 13:40:14 -0600

link-grammar (4.2.5-1) unstable; urgency=high

  * New upstream release.
    - Fixes boundary in separate_word() function.
      CVE-2007-5395 and Secunia advisory SA27340
      (Closes: #450695)
    - Adds new API for extracting constituents.
  * Removed all local patches as they've all been accepted upstream.

 -- Ken Bloom <kbloom@gmail.com>  Fri, 09 Nov 2007 14:19:10 -0600

link-grammar (4.2.2-4) unstable; urgency=low

  * Fix stupid bashism in debian/rules (Closes: #376478)
  * Return to quilt for patches to more easily manage the changes
    introduced in the last two versions. Next time, I won't remove
    quilt, even when I have no patches.
  * Bump standards-version to 3.7.2
  * Fix homepage pseudo-field to have 2 spaces at the beginning so p.d.o
    can parse it correctly.

 -- Ken Bloom <kbloom@gmail.com>  Tue,  4 Jul 2006 22:49:41 -0500

link-grammar (4.2.2-3) unstable; urgency=low

  * Get my fix for the sentence length error message correct so it
    doesn't leak memory.
  * Import prototype for safe_strdup() to prevent crashes on amd64
    (Closes: #366490) Thanks to Dann Fraizer, and his script on the
    AMD64 buildd.

 -- Ken Bloom <kbloom@gmail.com>  Tue,  9 May 2006 08:35:22 -0500

link-grammar (4.2.2-2) unstable; urgency=low

  * Updated manpage to be more clear. Thanks to Maru Dubshinki
    <marudubshinki@gmail.com> (Closes: #364532)
  * Merged patch from upstream CVS to handle unset $LANG.
    (Closes: #364738)
  * Don't use sentence to generate error message after deleting it.
    (Closes: #366136). I have not done an audit to find other cases of
    this.

 -- Ken Bloom <kbloom@gmail.com>  Sun, 23 Apr 2006 22:44:03 -0500

link-grammar (4.2.2-1) unstable; urgency=low

  * New Upstream Release
  * Eliminate quilt use, and eliminate all patches.
    - Patches for dictionary selection were merged upstream
    - Rename the commandline parser using debian/rules rather than
      reautotooling.
  * Install -T is broken despite being documented in the man page. Work
    around that in debian/rules (Closes: #359280)

 -- Ken Bloom <kbloom@gmail.com>  Tue, 18 Apr 2006 11:27:15 -0500

link-grammar (4.1.3-2) unstable; urgency=low

  * The commandline parser (and not the search path) should have been
    patched to fix dictionary selection (Closes: 352165)
  * Use alternatives system to manage the default dictionary for the
    command-line parser

 -- Ken Bloom <kbloom@gmail.com>  Wed, 15 Feb 2006 09:01:45 -0600

link-grammar (4.1.3-1) unstable; urgency=low

  * Initial release Closes: #337277
  * Patches:
   - dictionary-path-references
     Since the dictionaries live in data/en, they don't need to refer to
     en/words as this would really mean data/en/en/words which doesn't
     exist.
   - binary-name
     Named this link-grammar instead of grammar-parse. Partly personal
     preference, and partly because if other kinds of grammar get added
     to Debian, this one shouldn't have the generic name.
   - default-english-dictionary
     Make link-grammar use the english dictionary in data/en as the
     default.  (Upstream stored the dictionary in data/en, but told the
     program to refer to the nonexistent one in data/)
   - acinclude_binreloc
     Needed to reautotoolize.
   - reautotoolize
     Reautotoolize to deal with the stuff done in binary-name and
     default-english-dictionary

 -- Ken Bloom <kbloom@gmail.com>  Thu,  3 Nov 2005 16:53:09 -0600
